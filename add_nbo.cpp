#include "add_nbo.h"
#include <arpa/inet.h>

void Usage(char **argv){
    printf("Usage:) %s <file1> <file2> \n", argv[0]);
    printf("sample:) %s a.bin b.bin \n", argv[0]);
}

uint32_t add_nbo(char *argv){
    FILE *f;
    uint32_t res_buf;
    int byte_len;
    f = fopen(argv, "r");
    if(f == NULL){
        printf("File OPEN Error \n");
        exit(1);
    }
    byte_len = fread(&res_buf, sizeof(res_buf), 1, f);
    fclose(f);
    if(byte_len != 1){
        printf("File READ Error\n");
        exit(1);
    }else{
        res_buf = ntohl(res_buf);
        return res_buf;
    }
}

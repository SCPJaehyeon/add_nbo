#include "add_nbo.h"

int main(int argc, char *argv[])
{
    if (argc != 3){
        Usage(argv);
        return 0;
    }
    uint32_t buf[2] = {add_nbo(argv[1]), add_nbo(argv[2])};
    uint32_t sum = buf[0] + buf[1];
    printf("%d(%#x) + %d(%#x) = %d(%#x) \n", buf[0], buf[0], buf[1], buf[1], sum, sum);
    
    return 0;
}
